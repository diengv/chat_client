import axios from "axios";

export const appService = axios.create({
    baseURL: import.meta.env.VITE_API_URL
})

export const appService2 = axios.create({
    baseURL: import.meta.env.VITE_API_URL2
})

class AppService {
    login(data) {
        return appService.post('api/auth/login', data)
    }
}

export const httpService = new AppService()
